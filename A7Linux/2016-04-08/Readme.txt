This package is diff from A7Linux-1.0.4-PC to A7Linux-1.1.2-PC.

Key command:
    # Get command list
    git log --pretty=format:'%H' --no-merges --author=@qti.qualcomm.com --author=@csr.com A7Linux-1.0.4-PC..A7Linux-1.1.2-PC
    
    # According every commit get patch files.
    git format-patch -s [commit] -o [output path]